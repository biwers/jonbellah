import React from 'react';
import Link from 'gatsby-link';
import Helmet from 'react-helmet';
import banner from '../images/header.png';
import cover1 from '../images/slowly.jpg';

import RightArrow from '../images/RightArrow';

const Index = () => (
  <div className="container flex flex-hor-center">
    <Helmet
      title="JonBellah.com"
      meta={[
        {
          name: 'description',
          content:
            'Jon is a front-end web developer, speaker, and occasional writer living in the beautiful city of Denver, Colorado.',
        },
      ]}
    />
    <div className="hero">
      <img className="banner" itemProp="image" src={banner} alt="Audrey Jolin" />
      
      <table CELLPADDING="2" CELLSPACING="2" WIDTH="100%" border="0">
  <tr>
    <td><img className="cover1" itemProp="image" src={cover1} width="300" height="300"  alt="Slowly" /></td>
    <td><iframe src="https://open.spotify.com/embed?uri=spotify:artist:29OkJf5rT0OCd9mjvL8yB3" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe></td>
  </tr>
  <tr>
    <td><Link to="#" className="hero__link">
        Download/Stream <RightArrow />
      </Link></td>
  </tr>
</table>
      
      
      <span className="hero__label">Hello, my name is</span>
      <h1 className="hero__title">Jon Bellah</h1>
      <p className="hero__bio">
        I am a front-end web developer, speaker, and occasional writer living in
        the beautiful city of Denver, Colorado.
      </p>
      <Link to="/articles/" className="hero__link">
        Check out my articles <RightArrow />
      </Link>
    </div>
  </div>
);

export default Index;